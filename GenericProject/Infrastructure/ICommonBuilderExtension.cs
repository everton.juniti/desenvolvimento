﻿using Domain.Dtos.Configurations;
using Domain.Dtos.Requests;
using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Repository.Repositories.EventTopic;
using Repository.Repositories.MessageQueue;
using Repository.Repositories.SqlServer;

namespace Infrastructure;

public static class ICommonBuilderExtension
{
    public static IServiceCollection AddMediator(this IServiceCollection services) => services
        .AddMediatR(settings => settings.RegisterServicesFromAssemblies(System.Reflection.Assembly.Load("Application")));

    public static IServiceCollection AddDbRepository(this IServiceCollection services) => services
        .AddScoped<IDBRepository, SqlServerRepository>();

    public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration) => services
        .AddDbContext<SqlServerDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("SqlServer")));

    public static IServiceCollection AddMqRepository(this IServiceCollection services) => services
        .AddSingleton<IMQRepository<MessageRequest>>(serviceProvider => 
            new RabbitMqRepository(serviceProvider.GetRequiredService<IOptions<MqSettings>>().Value, 
                                    serviceProvider.GetRequiredService<ILogger<RabbitMqRepository>>()));

    public static IServiceCollection AddDummyMqRepository(this IServiceCollection services) => services
        .AddSingleton<IMQRepository<MessageRequest>, DummyRabbitMqRepository>();

    public static IServiceCollection AddTopicRepository(this IServiceCollection services) => services
        .AddSingleton<IEventRepository>(serviceProvider => 
            new KafkaRepository(serviceProvider.GetRequiredService<IOptions<TopicSettings>>().Value,
                                serviceProvider.GetRequiredService<ILogger<KafkaRepository>>()));

    public static IServiceCollection AddDummyTopicRepository(this IServiceCollection services) => services
        .AddSingleton<IEventRepository, DummyKafkaRepository>();
}
