﻿using Domain.Dtos.Configurations;
using Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace QueueWorker;

internal class Program
{
    static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .Build();

        return Host.CreateDefaultBuilder(args)
            .ConfigureServices((hostContext, services) => services
                .Configure<MqSettings>(configuration.GetSection("MqSettings"))
                .Configure<TopicSettings>(configuration.GetSection("TopicSettings"))
                .AddDbRepository()
                .AddDbContext(configuration)
                .AddMqRepository()
                .AddDummyTopicRepository()
                .AddMediator()
                .AddHostedService<MessageWorker>()
            );
    }
}