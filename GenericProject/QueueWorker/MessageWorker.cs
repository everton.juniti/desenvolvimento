﻿using Domain.Dtos.Requests;
using Domain.Interfaces.Repositories;
using MediatR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace QueueWorker;
internal class MessageWorker(ILogger<MessageWorker> logger, IMQRepository<MessageRequest> iMQRepository, IMediator mediator) : BackgroundService
{
    private readonly ILogger<MessageWorker> _logger = logger;
    private readonly IMQRepository<MessageRequest> _iMQRepository = iMQRepository;
    private readonly IMediator _mediator = mediator;

    public override Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("MessageWorker starting at: {time}", DateTimeOffset.Now);

        return base.StartAsync(cancellationToken);
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {

        int retryCount = 0;
        int maxRetryCount = 10;
        TimeSpan delay = TimeSpan.FromSeconds(1);

        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                _iMQRepository.Receive();

                var notification = _iMQRepository.GetMessage();
                if (notification != null)
                    await _mediator.Publish(notification, cancellationToken);

                retryCount = 0;
            }
            catch (Exception ex)
            {
                if (++retryCount >= maxRetryCount)
                {
                    _logger.LogError(ex, "[MessageWorker] Maximum number of retries exceeded");
                    break;
                }

                _logger.LogError(ex, "[MessageWorker] Error occurred. Retrying in {Delay} seconds", delay.TotalSeconds);
                await Task.Delay(delay, cancellationToken);
                delay = TimeSpan.FromSeconds(Math.Pow(2, retryCount));
            }
        }
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("MessageWorker stopping at: {time}", DateTimeOffset.Now);

        return base.StopAsync(cancellationToken);
    }
}
