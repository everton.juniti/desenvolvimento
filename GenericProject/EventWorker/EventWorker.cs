﻿using Domain.Dtos.Repositories;
using Domain.Dtos.Requests;
using Domain.Interfaces.Repositories;
using MediatR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace EventWorker;

internal class EventWorker(ILogger<EventWorker> logger, IDBRepository repository, IMediator mediator) : BackgroundService
{
    private readonly ILogger<EventWorker> _logger = logger;
    private readonly IDBRepository _repository = repository;
    private readonly IMediator _mediator = mediator;

    public override Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("EventWorker running at: {time}", DateTimeOffset.Now);

        return base.StartAsync(cancellationToken);
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        int retryCount = 0;
        int maxRetryCount = 10;
        int maxDelay = 300;
        TimeSpan delay = TimeSpan.FromSeconds(1);

        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                _logger.LogInformation("EventWorker processing new events at: {time}", DateTimeOffset.Now);

                var events = await _repository.GetAllAsync<Outbox>();

                foreach (var eventItem in events)
                    if (await _mediator.Send(new EventRequest(eventItem), cancellationToken))
                        await _repository.DeleteAsync<Outbox>(eventItem.Id);
                
                retryCount = 0;

                if (events.Count() == 0)
                    delay = (delay.TotalSeconds < maxDelay) ? delay + TimeSpan.FromSeconds(1) : TimeSpan.FromSeconds(maxDelay);
                else
                    delay = TimeSpan.FromSeconds(1);

                await Task.Delay(delay, cancellationToken);
            }
            catch (Exception ex)
            {
                if (++retryCount >= maxRetryCount)
                {
                    _logger.LogError(ex, "[EventWorker] Maximum number of retries exceeded");
                    break;
                }

                _logger.LogError(ex, "[EventWorker] Error occurred. Retrying in {Delay} seconds", delay.TotalSeconds);
                await Task.Delay(delay, cancellationToken);
                delay = TimeSpan.FromSeconds(Math.Pow(2, retryCount));
            }
        }
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("EventWorker stopping at: {time}", DateTimeOffset.Now);

        return base.StopAsync(cancellationToken);
    }
}