﻿using Api.Dtos;
using Domain.Dtos.Repositories;
using Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

/// <summary>
/// CRUD operations for books
/// </summary>
[ApiController]
[Route("[controller]")]
public class BookController : ControllerBase
{
    private readonly ILogger<BookController> _logger;

    private readonly IServiceScopeFactory _serviceScopeFactory;

    /// <summary>
    /// The constructor of the class
    /// </summary>
    /// <param name="logger">A logger object to log everything in the class</param>
    /// <param name="serviceScopeFactory">A service scope factory object to use MediatR to Send requests</param>
    public BookController(ILogger<BookController> logger, IServiceScopeFactory serviceScopeFactory)
    {
        _logger = logger;
        _serviceScopeFactory = serviceScopeFactory;
    }

    /// <summary>
    /// Creates a new book
    /// </summary>
    /// <param name="payload">A BookRequest payload with book data</param>
    /// <returns>The created book with it's Id as an BookResponse object</returns>
    [HttpPost(Name = "CreateBook")]
    public ActionResult<BookResponse> CreateAsync([FromBody] BookRequest payload)
    {
        var book = new Book { Title = payload.Title, Author = payload.Author, Description = payload.Description };

        var response = SendAsync(book, EnumCrudOperation.Create).GetAwaiter().GetResult();

        if (response == null)
            return NoContent();

        var bookResponse = response.Books.FirstOrDefault();

        if (bookResponse == null)
            return NoContent();

        var createdBook = new BookResponse
        {
            Id = bookResponse.Id,
            Title = bookResponse.Title,
            Author = bookResponse.Author,
            Description = bookResponse.Description,
        };

        return Ok(createdBook);
    }

    /// <summary>
    /// Lists all books
    /// </summary>
    /// <returns>A list of all books from the database as a List of BookResponse object</returns>
    [HttpGet(Name = "GetBooks")]
    public ActionResult<IEnumerable<BookResponse>> Get()
    {
        var response = SendAsync(null, EnumCrudOperation.List).GetAwaiter().GetResult();

        if (response == null)
            return NoContent();

        var bookResponse = response.Books;

        if (bookResponse == null || bookResponse.Count == 0)
            return NoContent();

        var list = bookResponse.Select(book => new BookResponse
        {
            Id = book.Id,
            Title = book.Title,
            Author = book.Author,
            Description = book.Description,
        });

        return Ok(list);
    }

    /// <summary>
    /// Return a book by it's Id
    /// </summary>
    /// <param name="id">A Guid representing a book Id</param>
    /// <returns>The book specified by it's Id as an BookResponse object</returns>
    [HttpGet("{id}", Name = "GetBook")]
    public ActionResult<BookResponse> Get(Guid id)
    {
        var foundBook = FindBook(id).GetAwaiter().GetResult();

        if (foundBook == null)
            return NotFound();

        var response = new BookResponse
        {
            Id = foundBook.Id,
            Title = foundBook.Title,
            Author = foundBook.Author,
            Description = foundBook.Description
        };

        return Ok(response);
    }

    /// <summary>
    /// Updates a book by it's Id and a BookRequest payload
    /// </summary>
    /// <param name="id">A Guid representing a book Id</param>
    /// <param name="payload">A BookRequest payload with book data</param>
    /// <returns>The book with it's updated data as an BookResponse object</returns>
    [HttpPut("{id}", Name = "UpdateBook")]
    public ActionResult<BookResponse> Update(Guid id, [FromBody] BookRequest payload)
    {
        var foundBook = FindBook(id).GetAwaiter().GetResult();

        if (foundBook == null)
            return NotFound();

        var book = new Book
        {
            Id = foundBook.Id,
            Title = payload.Title,
            Author = payload.Author,
            Description = payload.Description,
            CreatedAt = foundBook.CreatedAt,
            UpdatedAt = DateTime.Now,
        };

        var response = SendAsync(book, EnumCrudOperation.Update).GetAwaiter().GetResult();

        if (response == null)
            return NotFound();

        var bookResponse = response.Books != null ? response.Books.FirstOrDefault() : null;

        if (bookResponse == null)
            return NotFound();

        var updatedBook = new BookResponse
        {
            Id = bookResponse.Id,
            Title = bookResponse.Title,
            Author = bookResponse.Author,
            Description = bookResponse.Description,
        };

        return Ok(updatedBook);
    }

    /// <summary>
    /// Deletes a book by it's Id
    /// </summary>
    /// <param name="id">A Guid representing a book Id</param>
    /// <returns>No content if the deletion was succeeded</returns>
    [HttpDelete("{id}", Name = "DeleteBook")]
    public ActionResult Delete(Guid id)
    {
        var book = new Book { Id = id };

        var response = SendAsync(book, EnumCrudOperation.Delete).GetAwaiter().GetResult();

        if (response == null)
            return NotFound();

        if (response.Books != null && response.Books.Count > 0)
            return NotFound();

        return NoContent();
    }

    private Task<Book?> FindBook(Guid id)
    {
        var book = new Book { Id = id };

        var response = SendAsync(book, EnumCrudOperation.Get).GetAwaiter().GetResult();

        if (response == null)
            return Task.FromResult<Book?>(null);

        var bookResponse = response.Books != null ? response.Books.FirstOrDefault() : null;

        if (bookResponse == null)
            return Task.FromResult<Book?>(null);

        return Task.FromResult<Book?>(bookResponse);
    }

    private async Task<Domain.Dtos.Responses.BookResponse> SendAsync(Book? book, EnumCrudOperation operation)
    {
        using var scope = _serviceScopeFactory.CreateScope();
        var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();
        var response = await mediator.Send(new Domain.Dtos.Requests.BookRequest
        {
            Data = book,
            CrudOperation = operation
        });

        return response;
    }
}