﻿namespace Api.Dtos;

/// <summary>
/// A response for a book
/// </summary>
public class BookResponse
{
    /// <summary>
    /// The Id of the book
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// The title of the book
    /// </summary>
    public required string Title { get; set; }

    /// <summary>
    /// The author of the book
    /// </summary>
    public required string Author { get; set; }

    /// <summary>
    /// The description of the book
    /// </summary>
    public required string Description { get; set; }
}