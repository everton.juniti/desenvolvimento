﻿using Domain.Dtos.Repositories;
using Domain.Dtos.Requests;
using Domain.Enums;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace Application.Handlers;

public sealed class MessageHandler(ILogger<MessageHandler> logger, IMediator mediator) : INotificationHandler<MessageRequest>
{
    private readonly ILogger<MessageHandler> _logger = logger;
    private readonly IMediator _mediator = mediator;

    public async Task Handle(MessageRequest notification, CancellationToken cancellationToken)
    {
        if (notification.Message != null)
        {
            try
            {
                var book = JsonSerializer.Deserialize<Book>(notification.Message);

                var request = new BookRequest
                {
                    CrudOperation = EnumCrudOperation.Create,
                    Data = book
                };

                var response = await _mediator.Send(request);

                _logger.LogInformation($"Book created: {response.Books[0].Id}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while processing message: {notification.Message}. Exception: {ex.Message}");
            }
        }
    }
}
