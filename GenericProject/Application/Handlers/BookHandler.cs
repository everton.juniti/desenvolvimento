﻿using Domain.Dtos.Repositories;
using Domain.Dtos.Requests;
using Domain.Dtos.Responses;
using Domain.Enums;
using Domain.Interfaces.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace Application.Handlers;

/// <summary>
/// The handler for the BookRequest
/// </summary>
public sealed class BookHandler : IRequestHandler<BookRequest, BookResponse>
{
    private readonly ILogger<BookHandler> _logger;

    private readonly IDBRepository _repository;

    private readonly Dictionary<EnumCrudOperation, Func<BookRequest, Task<BookResponse>>> _operations;

    /// <summary>
    /// The constructor of the class
    /// </summary>
    /// <param name="repository">A repository object to manage CRUD operations within a database</param>
    /// <param name="logger">A logger object to log everything in the class</param>
    public BookHandler(ILogger<BookHandler> logger, IDBRepository repository)
    {
        _logger = logger;
        _repository = repository;

        _operations = new Dictionary<EnumCrudOperation, Func<BookRequest, Task<BookResponse>>>
        {
            { EnumCrudOperation.Create, Create },
            { EnumCrudOperation.Update, Update },
            { EnumCrudOperation.Delete, Delete },
            { EnumCrudOperation.List, List },
            { EnumCrudOperation.Get, GetById }
        };
    }

    /// <summary>
    /// Handles the BookRequest
    /// </summary>
    /// <param name="bookRequest">A BookRequest object with book data</param>
    /// <param name="cancellationToken">The cancellation token to cancel a async running task</param>
    /// <returns></returns>
    public async Task<BookResponse> Handle(BookRequest bookRequest, CancellationToken cancellationToken)
    {
        _logger.LogInformation($"Handling book request: {bookRequest.CrudOperation}");

        return await _operations[bookRequest.CrudOperation](bookRequest);
    }

    private async Task<BookResponse> Create(BookRequest bookRequest)
    {
        _logger.LogInformation($"Creating book: {bookRequest.Data.Title}");

        var book = new Book
        {
            Author = bookRequest.Data.Author,
            Title = bookRequest.Data.Title,
            Description = bookRequest.Data.Description,
            CreatedAt = DateTime.Now,
            UpdatedAt = null
        };
        var bookCreated = await _repository.CreateAsync(book);

        _logger.LogInformation($"Book created: {bookCreated.Id}");

        var outboxCreated = await _repository.CreateAsync(new Outbox
        {
            Id = Guid.NewGuid(),
            Outbox_Type_Id = (byte)EnumOutboxType.Book,
            Outbox_Event_Id = (byte)EnumOutboxEvent.BookCreated,
            Message = JsonSerializer.Serialize(bookCreated)
        });

        _logger.LogInformation($"Outbox created: {outboxCreated.Id}");

        return new BookResponse { Books = [bookCreated] };
    }

    private async Task<BookResponse> List(BookRequest bookRequest)
    {
        _logger.LogInformation("Listing all books");

        var books = await _repository.GetAllAsync<Book>();

        _logger.LogInformation($"Books listed: {books.Count()}");

        return new BookResponse { Books = books.ToList() };
    }

    private async Task<BookResponse> GetById(BookRequest bookRequest)
    {
        _logger.LogInformation($"Getting book by Id: {bookRequest.Data.Id}");

        var book = await _repository.GetByIdAsync<Book>(bookRequest.Data.Id);

        if (book == null)
        {
            _logger.LogInformation($"Book not found: {bookRequest.Data.Id}");
            return new BookResponse { Books = [] };
        }

        _logger.LogInformation($"Book found: {book.Title}");

        return new BookResponse { Books = [book] };
    }

    private async Task<BookResponse> Update(BookRequest bookRequest)
    {
        _logger.LogInformation($"Updating book: {bookRequest.Data.Title}");

        var updated = await _repository.UpdateAsync<Book>(bookRequest.Data);

        if (updated == false)
        {
            _logger.LogInformation($"Book not found: {bookRequest.Data.Id}");
            return new BookResponse { Books = [] };
        }   
        else
        {
            _logger.LogInformation($"Book updated: {bookRequest.Data.Title}");

            var outboxCreated = await _repository.CreateAsync(new Outbox
            {
                Id = Guid.NewGuid(),
                Outbox_Type_Id = (byte)EnumOutboxType.Book,
                Outbox_Event_Id = (byte)EnumOutboxEvent.BookUpdated,
                Message = JsonSerializer.Serialize(bookRequest.Data)
            });

            _logger.LogInformation($"Outbox created: {outboxCreated.Id}");

            return new BookResponse { Books = [bookRequest.Data] };
        }
    }

    private async Task<BookResponse> Delete(BookRequest bookRequest)
    {
        _logger.LogInformation($"Deleting book: {bookRequest.Data.Id}");

        var deleted = await _repository.DeleteAsync<Book>(bookRequest.Data.Id);

        if (deleted == true)
        {
            _logger.LogInformation($"Book deleted: {bookRequest.Data.Id}");

            var outboxCreated = await _repository.CreateAsync(new Outbox
            {
                Id = Guid.NewGuid(),
                Outbox_Type_Id = (byte)EnumOutboxType.Book,
                Outbox_Event_Id = (byte)EnumOutboxEvent.BookDeleted,
                Message = JsonSerializer.Serialize(bookRequest.Data)
            });

            _logger.LogInformation($"Outbox created: {outboxCreated.Id}");

            return new BookResponse { Books = [] };
        }   
        else
        {
            _logger.LogInformation($"Book not found: {bookRequest.Data.Id}");
            return new BookResponse { Books = [bookRequest.Data] };
        }   
    }
}