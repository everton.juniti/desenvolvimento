﻿using Domain.Dtos.Requests;
using Domain.Dtos.Wrappers;
using Domain.Interfaces.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Application.Handlers;

public sealed class EventHandler(ILogger<EventHandler> logger, IEventRepository eventRepository) : IRequestHandler<EventRequest, bool>
{
    private readonly ILogger<EventHandler> _logger = logger;
    private readonly IEventRepository _eventRepository = eventRepository;

    public Task<bool> Handle(EventRequest notification, CancellationToken cancellationToken)
    {
        return _eventRepository.ProduceAsync(new EventWrapper(notification.Outbox.GetType().Name, notification.Outbox.ToString()));
    }
}