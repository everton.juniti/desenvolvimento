﻿using Domain.Dtos.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Repository.Repositories.SqlServer;

public class SqlServerDbContext : DbContext
{
    public SqlServerDbContext(DbContextOptions<SqlServerDbContext> options) : base(options)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
    }

    public DbSet<Book> Books { get; set; }

    public DbSet<Outbox> OutBoxes { get; set; }
}
