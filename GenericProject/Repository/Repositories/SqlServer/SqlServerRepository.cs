﻿using Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Repository.Repositories.SqlServer;

public class SqlServerRepository(SqlServerDbContext context) : IDBRepository
{
    private readonly SqlServerDbContext _context = context;

    public async Task<T> CreateAsync<T>(T entity) where T : class
    {
        _context.Set<T>().Add(entity);
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<bool> DeleteAsync<T>(Guid id) where T : class
    {
        var entity = await _context.Set<T>().FindAsync(id);
        if (entity == null)
        {
            return false;
        }

        _context.Set<T>().Remove(entity);
        await _context.SaveChangesAsync();

        return true;
    }

    public async Task<IEnumerable<T>> GetAllAsync<T>() where T : class
    {
        return await _context.Set<T>().ToListAsync();
    }

    public async Task<T?> GetByIdAsync<T>(Guid id) where T : class
    {
        return await _context.Set<T>().FindAsync(id);
    }

    public async Task<bool> UpdateAsync<T>(T entity) where T : class
    {
        _context.Set<T>().Update(entity);
        var updated = await _context.SaveChangesAsync();
        return updated > 0;
    }
}