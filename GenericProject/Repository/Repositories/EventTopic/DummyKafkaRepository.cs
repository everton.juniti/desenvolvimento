﻿using Domain.Dtos.Wrappers;
using Domain.Interfaces.Repositories;

namespace Repository.Repositories.EventTopic;

public class DummyKafkaRepository : IEventRepository
{
    public Task<bool> ProduceAsync(EventWrapper message)
    {
        return Task.FromResult(true);
    }
}