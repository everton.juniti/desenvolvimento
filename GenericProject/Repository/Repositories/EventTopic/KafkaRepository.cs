﻿using Confluent.Kafka;
using Domain.Dtos.Configurations;
using Domain.Dtos.Wrappers;
using Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;

namespace Repository.Repositories.EventTopic;

public class KafkaRepository(TopicSettings topicSettings, ILogger<KafkaRepository> logger) : IEventRepository
{
    private readonly TopicSettings _topicSettings = topicSettings;
    private readonly ILogger<KafkaRepository> _logger = logger;

    public async Task<bool> ProduceAsync(EventWrapper message)
    {
        _logger.LogInformation($"Producing message: {message.Message}");

        var config = new ProducerConfig { BootstrapServers = _topicSettings.BootstrapServers };

        using (var producer = new ProducerBuilder<Null, string>(config).Build())
        {
            try
            {
                var result = await producer.ProduceAsync(_topicSettings.TopicName, new Message<Null, string> { Value = message.Message });
                _logger.LogInformation($"Delivered '{result.Value}' to {result.TopicPartitionOffset}");
                return true;
            }
            catch (ProduceException<Null, string> e)
            {
                _logger.LogError($"Delivery failed: {e.Error.Reason}");
            }
        }

        return false;
    }
}
