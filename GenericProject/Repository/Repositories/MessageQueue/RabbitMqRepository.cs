﻿using Domain.Dtos.Configurations;
using Domain.Dtos.Requests;
using Domain.Dtos.Wrappers;
using Domain.Interfaces.Repositories;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading.Channels;

namespace Repository.Repositories.MessageQueue;

public class RabbitMqRepository(MqSettings mqSettings, ILogger<RabbitMqRepository> logger) : IMQRepository<MessageRequest>, IDisposable
{
    private readonly MqSettings _mqSettings = mqSettings;
    private readonly ILogger<RabbitMqRepository> _logger = logger;
    private readonly Channel<MessageWrapper> _messageChannel = Channel.CreateUnbounded<MessageWrapper>();

    private IConnection _connection;
    private IModel _channel;

    public void Receive()
    {
        Connect();

        if (_channel != null && _channel.IsOpen)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                _logger.LogInformation($"Received message: {message}");
                _messageChannel.Writer.TryWrite(new MessageWrapper { Message = message, DeliveryTag = ea.DeliveryTag });
            };

            _channel.BasicConsume(queue: _mqSettings.QueueName, autoAck: false, consumer: consumer);
        }
    }

    public MessageRequest? GetMessage()
    {
        if (_messageChannel.Reader.Count > 0)
            if (_messageChannel.Reader.TryRead(out var messageWithDeliveryTag))
            {
                _channel.BasicAck(messageWithDeliveryTag.DeliveryTag, false);
                return new MessageRequest(Message: messageWithDeliveryTag.Message);
            }                

        return null;
    }

    public void Send(string message)
    {
        Connect();

        if (_channel != null && _channel.IsOpen)
        {
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: string.Empty,
                                    routingKey: "hello",
                                    basicProperties: null,
                                    body: body);

            _logger.LogInformation($"Sent message: {message}");
        }
    }

    private void Connect()
    {
        if (_connection == null || _connection.IsOpen == false)
        {
            var factory = new ConnectionFactory
            {
                VirtualHost = _mqSettings.VirtualHost,
                HostName = _mqSettings.HostName,
                Port = _mqSettings.PortNumber,
                UserName = _mqSettings.Username,
                Password = _mqSettings.Password
            };
            _connection = factory.CreateConnection();
        }

        if (_channel == null || _channel.IsOpen == false)
        {
            _channel = _connection.CreateModel();

            _channel.QueueDeclare(queue: _mqSettings.QueueName,
                                    durable: true,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);
        }
    }


    private void Disconnect()
    {
        if (_channel != null && _channel.IsOpen)
            _channel.Close();

        if (_connection != null && _connection.IsOpen)
            _connection.Close();
    }

    public void Dispose()
    {
        Disconnect();
        GC.SuppressFinalize(this);
    }
}