﻿using Domain.Dtos.Requests;
using Domain.Interfaces.Repositories;

namespace Repository.Repositories.MessageQueue;

public class DummyRabbitMqRepository : IMQRepository<MessageRequest>
{
    public void Receive()
    {
    }

    public void Send(string message)
    {
    }

    public MessageRequest? GetMessage()
    {
        return null;
    }
}