﻿namespace Domain.Enums;

public enum EnumCrudOperation
{
    Create,
    List,
    Get,
    Update,
    Delete
}