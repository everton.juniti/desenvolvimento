﻿namespace Domain.Enums;

public enum EnumOutboxEvent
{
    BookCreated = 1,
    BookUpdated = 2,
    BookDeleted = 3
}
