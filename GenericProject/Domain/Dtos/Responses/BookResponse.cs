﻿using Domain.Dtos.Repositories;

namespace Domain.Dtos.Responses;

public class BookResponse
{
    public List<Book>? Books { get; set; }
}