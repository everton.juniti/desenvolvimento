﻿namespace Domain.Dtos.Configurations;

public class TopicSettings
{
    public required string BootstrapServers { get; set; }

    public required string TopicName { get; set; }
}
