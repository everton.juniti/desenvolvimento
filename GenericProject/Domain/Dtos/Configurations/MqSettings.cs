﻿namespace Domain.Dtos.Configurations;
public class MqSettings
{
    public required string VirtualHost { get; set; }

    public required string HostName { get; set; }

    public required int PortNumber { get; set; }

    public required string QueueName { get; set; }

    public required string Username { get; set; }

    public required string Password { get; set; }
}