﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Dtos.Repositories;

[Table("Outbox")]
public record Outbox
{
    [Key]
    [Column("Id")]
    public Guid Id { get; init; }

    [Required]
    [Column("Outbox_Type_Id")]
    public required byte Outbox_Type_Id { get; init; }

    [Required]
    [Column("Outbox_Event_Id")]
    public required byte Outbox_Event_Id { get; init; }

    [Required]
    [Column("Message")]
    public required string Message { get; init; }
}
