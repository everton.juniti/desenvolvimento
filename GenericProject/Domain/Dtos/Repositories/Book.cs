﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Domain.Dtos.Repositories;

[Table("Book")]
public record Book
{
    [Key]
    [Column("Id")]
    [JsonPropertyName("id")]
    public Guid Id { get; init; }

    [Required]
    [Column("Title")]
    [JsonPropertyName("title")]
    public string? Title { get; init; }

    [Required]
    [Column("Author")]
    [JsonPropertyName("author")]
    public string? Author { get; init; }

    [Required]
    [Column("Description")]
    [JsonPropertyName("description")]
    public string? Description { get; init; }

    [Required]
    [Column("Created_At")]
    public DateTime CreatedAt { get; init; }

    [Column("Updated_At")]
    public DateTime? UpdatedAt { get; init; }
}