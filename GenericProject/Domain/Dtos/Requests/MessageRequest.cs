﻿using MediatR;

namespace Domain.Dtos.Requests;

public record MessageRequest(string Message) : INotification
{
}
