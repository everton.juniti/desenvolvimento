﻿using Domain.Dtos.Repositories;
using Domain.Dtos.Responses;
using Domain.Enums;
using MediatR;

namespace Domain.Dtos.Requests;

public class BookRequest : IRequest<BookResponse>
{
    public Book? Data { get; set; }

    public EnumCrudOperation CrudOperation { get; set; }

    public void SetRequestType(EnumCrudOperation crudOperation)
    {
        this.CrudOperation = crudOperation;
    }
}
