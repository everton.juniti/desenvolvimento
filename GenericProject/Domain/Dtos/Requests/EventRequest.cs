﻿using Domain.Dtos.Repositories;
using MediatR;

namespace Domain.Dtos.Requests;

public record EventRequest(Outbox Outbox) : IRequest<bool>
{
}