﻿namespace Domain.Dtos.Wrappers;

public class MessageWrapper
{
    public required string Message { get; set; }
    public ulong DeliveryTag { get; set; }
}
