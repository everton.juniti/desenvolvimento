﻿namespace Domain.Dtos.Wrappers;

public record EventWrapper(string EventName, string Message)
{
}