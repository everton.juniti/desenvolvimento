﻿namespace Domain.Interfaces.Repositories;

public interface IDBRepository
{
    Task<IEnumerable<T>> GetAllAsync<T>() where T : class;

    Task<T?> GetByIdAsync<T>(Guid id) where T : class;

    Task<T> CreateAsync<T>(T entity) where T : class;

    Task<bool> UpdateAsync<T>(T entity) where T : class;

    Task<bool> DeleteAsync<T>(Guid id) where T : class;
}