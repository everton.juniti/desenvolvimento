﻿using Domain.Dtos.Requests;

namespace Domain.Interfaces.Repositories;
public interface IMQRepository<T>
{
    void Send(string message);

    void Receive();

    MessageRequest? GetMessage();
}