﻿using Domain.Dtos.Wrappers;

namespace Domain.Interfaces.Repositories;

public interface IEventRepository
{
    Task<bool> ProduceAsync(EventWrapper message);
}