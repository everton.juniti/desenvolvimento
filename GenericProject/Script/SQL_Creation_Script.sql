CREATE TABLE Book
(
	Id UNIQUEIDENTIFIER NOT NULL, 
	Title VARCHAR(150) NOT NULL, 
	Author VARCHAR(150) NOT NULL, 
	Description VARCHAR(250) NOT NULL,
	Created_At DATETIME2 NOT NULL,
	Updated_At DATETIME2 NULL, 
	CONSTRAINT PK_Book PRIMARY KEY CLUSTERED(Id)
);

CREATE TABLE Outbox_Type 
(
	Id TINYINT NOT NULL, 
	Description VARCHAR(50), 
	CONSTRAINT PK_Outbox_Type PRIMARY KEY CLUSTERED(Id)
);

INSERT INTO Outbox_Type (Id, Description) 
VALUES (1, 'Book');

CREATE TABLE Outbox_Event
(
	Id TINYINT NOT NULL,
	Description VARCHAR(50),
	CONSTRAINT PK_Outbox_Event PRIMARY KEY CLUSTERED(Id)
);

INSERT INTO Outbox_Event (Id, Description)
VALUES (1, 'Book_Created');

INSERT INTO Outbox_Event (Id, Description) 
VALUES (2, 'Book_Updated');

INSERT INTO Outbox_Event (Id, Description) 
VALUES (3, 'Book_Deleted');

CREATE TABLE Outbox 
(
	Id UNIQUEIDENTIFIER NOT NULL, 
	Outbox_Type_Id TINYINT NOT NULL, 
	Outbox_Event_Id TINYINT NOT NULL, 
	Message NVARCHAR(MAX) NOT NULL, 
	CONSTRAINT PK_Outbox PRIMARY KEY CLUSTERED(Id),
	CONSTRAINT FK_Outbox_Type FOREIGN KEY (Outbox_Type_Id) REFERENCES Outbox_Type(Id),
	CONSTRAINT FK_Outbox_Event FOREIGN KEY (Outbox_Event_Id) REFERENCES Outbox_Event(Id)
);