package br.com.ogura.restapidemo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApiDemoController {
	
	@GetMapping
	public @ResponseBody ResponseEntity<String> get() {
		return ResponseEntity.ok("OK");
	}
	
}
