package br.com.evertonogura;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Robot;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Application {
	
	private static boolean MOVE_MOUSE = true;
	public static final int SECONDS = 1000;
	public static final int SECONDS_MOUSE_MOVE = 60000;
	public static final int MAX_Y = 400;
	public static final int MAX_X = 400;
	
	public static void main(String[] args) {
		addSystemTrayIcon();
		
		try {
			mouseMove();
		} catch (AWTException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static void addSystemTrayIcon() {
		if (!SystemTray.isSupported()) {
			System.out.println("SystemTray n�o � suportado");
			return;
		}
		
		final PopupMenu popup = new PopupMenu();
		final Image image = Toolkit.getDefaultToolkit().getImage("resources/Cursor_Icon.png");
		final TrayIcon trayIcon = new TrayIcon(image);
		final SystemTray tray = SystemTray.getSystemTray();
		
		MenuItem ativarItem = new MenuItem("Ativar");
		MenuItem desativarItem = new MenuItem("Desativar");
		MenuItem sairItem = new MenuItem("Sair");
		
		ativarItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MOVE_MOUSE = true;
			}
		});
		
		desativarItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MOVE_MOUSE = false;
			}
		});
		
		sairItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		popup.add(ativarItem);
		popup.add(desativarItem);
		popup.add(sairItem);
		
		trayIcon.setPopupMenu(popup);
		
		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			System.out.println("TrayIcon n�o p�de ser adicionado.");
		}
	}
	
	private static void mouseMove() throws AWTException, InterruptedException {
		Robot robot = new Robot();
		Random random = new Random();
		
		while (true) {
			while(MOVE_MOUSE) {
				robot.mouseMove(random.nextInt(MAX_X), random.nextInt(MAX_Y));
				Thread.sleep(SECONDS_MOUSE_MOVE);
			}
			Thread.sleep(SECONDS);
		}
	}

}
