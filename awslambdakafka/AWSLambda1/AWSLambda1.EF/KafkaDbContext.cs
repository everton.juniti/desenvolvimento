﻿using AWSLambda1.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace AWSLambda1.EF
{
    public class KafkaDbContext : DbContext
    {
        public KafkaDbContext(DbContextOptions options) : base(options) { }

        public DbSet<TesteKafka> TesteKafka { get; set; }
    }
}
