﻿using AWSLambda1.Abstractions;

namespace AWSLambda1.EF
{
    public class TesteKafkaRepository : ITesteKafkaRepository
    {
        public KafkaDbContext Context { get; }

        public TesteKafkaRepository(KafkaDbContext context)
        {
            Context = context;
        }

        public void Add(TesteKafka content)
        {
            Context.TesteKafka.Add(content);
            Context.SaveChanges();
        }
    }
}
