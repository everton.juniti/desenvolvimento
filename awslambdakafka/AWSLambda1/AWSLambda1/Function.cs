using Amazon.Lambda.Core;
using AWSLambda1.Abstractions;
using AWSLambda1.DI;
using AWSLambda1.EF;
using Microsoft.Extensions.DependencyInjection;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace AWSLambda1
{
    public class Function
    {
        // Repository
        public ITesteKafkaRepository TesteKafkaRepository;

        public string FunctionHandler(object input, ILambdaContext context)
        {
            // Get dependency resolver
            var resolver = new DependencyResolver(ConfigureServices);

            // Get products repo
            TesteKafkaRepository = resolver.ServiceProvider.GetService<ITesteKafkaRepository>();

            TesteKafkaRepository.Add(new TesteKafka { Texto = input.ToString() });
            return "OK";
        }

        // Register services with DI system
        private void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ITesteKafkaRepository, TesteKafkaRepository>();
        }
    }
}
