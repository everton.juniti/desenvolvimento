﻿using System;

namespace AWSLambda1.Configuration
{
    public static class Constants
    {
        public static class EnvironmentVariables
        {
            public const string ConsoleAppCoreEnvironment = "CONSOLEAPPCORE_ENVIRONMENT";
        }

        public static class Environments
        {
            public const string Production = "Production";
        }
    }
}
