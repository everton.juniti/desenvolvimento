﻿using AWSLambda1.DI;
using Microsoft.EntityFrameworkCore.Design;
using System.IO;

namespace AWSLambda1.EF.Design
{
    public class KafkaDbContextFactory : IDesignTimeDbContextFactory<KafkaDbContext>
    {
        public KafkaDbContext CreateDbContext(string[] args)
        {
            // Get DbContext from DI system
            var resolver = new DependencyResolver
            {
                CurrentDirectory = Path.Combine(Directory.GetCurrentDirectory(), "../AWSLambda1")
            };
            return resolver.ServiceProvider.GetService(typeof(KafkaDbContext)) as KafkaDbContext;
        }
    }
}
