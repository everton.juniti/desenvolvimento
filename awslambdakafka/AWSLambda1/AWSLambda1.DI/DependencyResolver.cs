﻿using AWSLambda1.Abstractions;
using AWSLambda1.Configuration;
using AWSLambda1.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AWSLambda1.DI
{
    public class DependencyResolver
    {
        public IServiceProvider ServiceProvider { get; }
        public string CurrentDirectory { get; set; }
        public Action<IServiceCollection> RegisterServices { get; }

        public DependencyResolver(Action<IServiceCollection> registerServices = null)
        {
            // Set up Dependency Injection
            var serviceCollection = new ServiceCollection();
            RegisterServices = registerServices;
            ConfigureServices(serviceCollection);
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            // Register env and config services
            services.AddTransient<IEnvironmentService, EnvironmentService>();
            services.AddTransient<IConfigurationService, ConfigurationService>
                (provider => new ConfigurationService(provider.GetService<IEnvironmentService>())
                {
                    CurrentDirectory = CurrentDirectory
                });

            // Register DbContext class
            services.AddTransient(provider =>
            {
                string server = Environment.GetEnvironmentVariable("DB_ENDPOINT");
                string database = Environment.GetEnvironmentVariable("DATABASE");
                string username = Environment.GetEnvironmentVariable("USER");
                string pwd = Environment.GetEnvironmentVariable("PASSWORD");
                
                var configService = provider.GetService<IConfigurationService>();
                var connectionString = String.Format(configService.GetConfiguration().GetConnectionString(nameof(KafkaDbContext)), server, database, username, pwd);
                var optionsBuilder = new DbContextOptionsBuilder<KafkaDbContext>();
                optionsBuilder.UseMySql(connectionString, builder => builder.MigrationsAssembly("AWSLambda1.EF.Design"));
                return new KafkaDbContext(optionsBuilder.Options);
            });

            // Register other services
            RegisterServices?.Invoke(services);
        }
    }
}
