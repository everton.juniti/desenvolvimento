﻿using Microsoft.Extensions.Configuration;

namespace AWSLambda1.Abstractions
{
    public interface IConfigurationService
    {
        IConfiguration GetConfiguration();
    }
}
