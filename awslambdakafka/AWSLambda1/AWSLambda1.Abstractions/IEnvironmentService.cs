﻿namespace AWSLambda1.Abstractions
{
    public interface IEnvironmentService
    {
        string EnvironmentName { get; set; }
    }
}
