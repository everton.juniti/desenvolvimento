﻿using Amazon.CDK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TemplateCdk
{
    sealed class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();
            new TemplateCdkStack(app, "TemplateCdkStack");
            app.Synth();
        }
    }
}
