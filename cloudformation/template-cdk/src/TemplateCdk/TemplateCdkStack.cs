using Amazon.CDK;
using Amazon.CDK.AWS.EC2;

namespace TemplateCdk
{
    public class TemplateCdkStack : Stack
    {
        internal TemplateCdkStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            SubnetConfiguration[] subnetConfiguration = { 
                new SubnetConfiguration
                {
                    CidrMask = 24,
                    Name = "ALBPublicSubnetAZ1",
                    SubnetType = SubnetType.PUBLIC
                },
                new SubnetConfiguration
                {
                    CidrMask = 24,
                    Name = "AppPrivateSubnetAZ1",
                    SubnetType = SubnetType.PRIVATE
                },
                new SubnetConfiguration
                {
                    CidrMask = 24,
                    Name = "AppPrivateSubnetAZ2",
                    SubnetType = SubnetType.PRIVATE
                },
                new SubnetConfiguration
                {
                    CidrMask = 24,
                    Name = "RdsIsolatedSubnetAZ1",
                    SubnetType = SubnetType.ISOLATED
                },
                new SubnetConfiguration
                {
                    CidrMask = 24,
                    Name = "RdsIsolatedSubnetAZ2",
                    SubnetType = SubnetType.ISOLATED
                }
            };

            var vpc = new Vpc(this, "Pre-Macthing SELIC VPC", new VpcProps {
                Cidr = "10.0.0.0/16",
                MaxAzs = 2,
                SubnetConfiguration = subnetConfiguration
            });
        }
    }
}
