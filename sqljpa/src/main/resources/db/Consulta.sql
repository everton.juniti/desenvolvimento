DECLARE @ColumnASimple CHAR(4) = 'AAAA' 
DECLARE @ColumnBSimple INTEGER = 1000

DECLARE @ColumnANew NVARCHAR(4000) = 'AAAA'
DECLARE @ColumnBNew INTEGER = 1000 

SET STATISTICS IO, TIME ON; 

SELECT * 
  FROM TB_Teste 
 WHERE ColumnA = @ColumnASimple AND ColumnB = @ColumnBSimple 

SELECT * 
  FROM TB_Teste_New 
 WHERE ColumnA = @ColumnANew AND ColumnB = @ColumnBNew 

SET STATISTICS IO, TIME OFF; 