package br.com.evertonjuniti.sqljpa.model;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SqlJpaModel implements Serializable {
	
	private static final long serialVersionUID = -3300138821124930637L;
	
	@JsonProperty(value = "id", required = true, index = 0)
	private long id;
	
	@JsonProperty(value = "columnA", required = true, index = 1)
	private String columnA;
	
	@JsonProperty(value = "columnB", required = true, index = 2)
	private int columnB;
	
	@JsonProperty(value = "columnC", required = true, index = 3)
	private UUID columnC;
	
	@JsonProperty(value = "columnD", required = true, index = 4)
	private int columnD;
	
	@JsonProperty(value = "columnE", required = false, index = 5)
	private String columnE;
	
}