package br.com.evertonjuniti.sqljpa.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.evertonjuniti.sqljpa.dto.TB_Teste;
import br.com.evertonjuniti.sqljpa.dto.TB_TesteNew;
import br.com.evertonjuniti.sqljpa.repository.SqlJpaQueryRepository;
import br.com.evertonjuniti.sqljpa.repository.SqlJpaNormalRepository;

@Service
public class SqlJpaService {
	
	@Autowired
	private SqlJpaNormalRepository sqlJpaNormalRepository;
	
	@Autowired
	private SqlJpaQueryRepository sqlJpaQueryRepository;
	
	public Optional<TB_Teste> findSimple(String columnA, int columnB) {
		return sqlJpaNormalRepository.findByCompositeKeyColumnAAndCompositeKeyColumnB(columnA, columnB);
	}
	
	public Optional<TB_TesteNew> findNew(String columnA, int columnB) {
		return sqlJpaQueryRepository.findByCompositeKeyColumnAAndCompositeKeyColumnB(columnA, columnB);
	}
	
}