package br.com.evertonjuniti.sqljpa.mapper;

import java.util.Optional;

import org.springframework.stereotype.Component;

import br.com.evertonjuniti.sqljpa.dto.TB_Teste;
import br.com.evertonjuniti.sqljpa.dto.TB_TesteNew;
import br.com.evertonjuniti.sqljpa.model.SqlJpaModel;

@Component
public class SqlJpaMapper {
	
	public SqlJpaModel mapSqlJpaNormal(Optional<TB_Teste> entity) {
		SqlJpaModel model = new SqlJpaModel();
		
		if (entity.isPresent()) {
			model.setId(entity.get().getCompositeKey().getId());
			model.setColumnA(entity.get().getCompositeKey().getColumnA());
			model.setColumnB(entity.get().getCompositeKey().getColumnB());
			model.setColumnC(entity.get().getColumnC());
			model.setColumnD(entity.get().getColumnD());
			model.setColumnE(entity.get().getColumnE());
		}
		
		return model;
	}
	
	public SqlJpaModel mapSqlJpaQuery(Optional<TB_TesteNew> entity) {
		SqlJpaModel model = new SqlJpaModel();
		
		if (entity.isPresent()) {
			model.setId(entity.get().getCompositeKey().getId());
			model.setColumnA(entity.get().getCompositeKey().getColumnA());
			model.setColumnB(entity.get().getCompositeKey().getColumnB());
			model.setColumnC(entity.get().getColumnC());
			model.setColumnD(entity.get().getColumnD());
			model.setColumnE(entity.get().getColumnE());
		}
		
		return model;
	}
	
}