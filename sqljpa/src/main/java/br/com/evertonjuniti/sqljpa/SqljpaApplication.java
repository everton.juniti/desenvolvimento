package br.com.evertonjuniti.sqljpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqljpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqljpaApplication.class, args);
	}

}