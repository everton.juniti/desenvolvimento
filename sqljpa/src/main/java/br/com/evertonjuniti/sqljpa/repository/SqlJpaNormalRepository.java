package br.com.evertonjuniti.sqljpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.evertonjuniti.sqljpa.dto.TB_Teste;
import br.com.evertonjuniti.sqljpa.dto.compositeKey.TB_TesteCompositeKey;

@Repository
public interface SqlJpaNormalRepository extends JpaRepository<TB_Teste, TB_TesteCompositeKey> {
	
	Optional<TB_Teste> findByCompositeKeyColumnAAndCompositeKeyColumnB(
			String columnA, int columnB);
	
}