package br.com.evertonjuniti.sqljpa.dto;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import br.com.evertonjuniti.sqljpa.dto.compositeKey.TB_TesteNewCompositeKey;
import lombok.Data;

@Entity
@Table(name = "TB_Teste_New")
@Data
public class TB_TesteNew implements Serializable {
	
	private static final long serialVersionUID = 3276812594932395098L;
	
	@EmbeddedId
	private TB_TesteNewCompositeKey compositeKey;
	
	@NotNull
	@Column(name = "ColumnC", nullable = false)
	@Type(type = "uuid-char")
	private UUID columnC;
	
	@NotNull
	@Column(name = "ColumnD", nullable = false)
	private int columnD;
	
	@Column(name = "ColumnE", nullable = true)
	private String columnE;
	
}