package br.com.evertonjuniti.sqljpa.dto.compositeKey;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Embeddable
@Data
public class TB_TesteCompositeKey implements Serializable {
	
	private static final long serialVersionUID = 1906187143732757172L;
	
	@NotNull
	@Column(name = "Id", nullable = false)
	private long id;
	
	@NotNull
	@Column(name = "ColumnA", nullable = false)
	private String columnA;
	
	@NotNull
	@Column(name = "ColumnB", nullable = false)
	private int columnB;
	
}