package br.com.evertonjuniti.sqljpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.evertonjuniti.sqljpa.dto.TB_TesteNew;
import br.com.evertonjuniti.sqljpa.dto.compositeKey.TB_TesteCompositeKey;

@Repository
public interface SqlJpaQueryRepository extends JpaRepository<TB_TesteNew, TB_TesteCompositeKey> {
	
	@Query(value = "SELECT t.Id, t.ColumnA, t.ColumnB, t.ColumnC, t.ColumnD, t.ColumnE "
			+ "FROM TB_Teste_New t "
			+ "WHERE t.ColumnA = CAST(:columnA AS VARCHAR(4)) "
			+ "AND t.ColumnB = :columnB", 
			nativeQuery = true)
	Optional<TB_TesteNew> findByCompositeKeyColumnAAndCompositeKeyColumnB(
			@Param("columnA") String columnA, @Param("columnB") int columnB);
	
}
