package br.com.evertonjuniti.sqljpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.evertonjuniti.sqljpa.business.SqlJpaBusiness;
import br.com.evertonjuniti.sqljpa.model.SqlJpaModel;

@RestController(value = "/teste")
public class SqlJpaController {
	
	@Autowired
	private SqlJpaBusiness business;
	
	@GetMapping(value = "/testeSqlJpaNormal")
	public @ResponseBody SqlJpaModel getSqlJpaNormal(
			@RequestParam(name = "columnA") String columnA,
			@RequestParam(name = "columnB") int columnB) {
		
		return business.getSqlJpaNormal(columnA, columnB);
		
	}
	
	@GetMapping(value = "/testeSqlJpaQuery")
	public @ResponseBody SqlJpaModel getSqlJpaQuery(
			@RequestParam(name = "columnA") String columnA,
			@RequestParam(name = "columnB") int columnB) {
		
		return business.getSqlJpaQuery(columnA, columnB);
		
	}
	
}