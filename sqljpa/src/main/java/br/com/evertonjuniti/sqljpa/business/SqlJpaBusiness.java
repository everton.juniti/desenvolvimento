package br.com.evertonjuniti.sqljpa.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.evertonjuniti.sqljpa.mapper.SqlJpaMapper;
import br.com.evertonjuniti.sqljpa.model.SqlJpaModel;
import br.com.evertonjuniti.sqljpa.service.SqlJpaService;

@Component
public class SqlJpaBusiness {
	
	@Autowired
	private SqlJpaMapper mapper;
	
	@Autowired
	private SqlJpaService service;
	
	public SqlJpaModel getSqlJpaNormal(String columnA, int columnB) {
		return mapper.mapSqlJpaNormal(service.findSimple(columnA, columnB));
	}
	
	public SqlJpaModel getSqlJpaQuery(String columnA, int columnB) {
		return mapper.mapSqlJpaQuery(service.findNew(columnA, columnB));
	}
	
}