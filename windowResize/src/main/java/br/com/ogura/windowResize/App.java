package br.com.ogura.windowResize;

import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.User32;

public class App 
{
	public static void main( String[] args )
    {
    	if (args.length == 3) {
    		System.out.println(args[0]);
    		System.out.println(args[1]);
    		System.out.println(args[2]);
    		
    		try {
    			String nomeForm = args[0];
        		int width = Integer.parseInt(args[1]);
        		int height = Integer.parseInt(args[2]);
        		
        		WinDef.HWND hWnd = User32.INSTANCE.FindWindow(null, nomeForm);
        		User32.INSTANCE.SetWindowPos(hWnd, null, 0, 0, width, height, 0x4000);
    		} catch (Exception ex) {
    			System.out.println("Ocorreu um problema ao tentar redimensionar a tela, use a aplicação com o seguinte comando: ");
        		System.out.println("java -jar windowResize.jar \"[Nome form]\" \"[Largura]\" \"[Altura]\"");
    		}
    	} else {
    		System.out.println("Número de parâmetros deve ser 3: ");
    		System.out.println("java -jar windowResize.jar \"[Nome form]\" \"[Largura]\" \"[Altura]\"");
    	}
    }
}
