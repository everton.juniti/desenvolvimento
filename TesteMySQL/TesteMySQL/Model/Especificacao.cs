﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteMySQL.Model
{
    [Table("especificacao")]
    public class Especificacao
    {
        [Key, Column("id")]
        [JsonProperty(PropertyName = "id", Required = Required.Default)]
        public long id { get; set; }

        [Column("id_negocio")]
        [JsonProperty(PropertyName = "id_negocio", Required = Required.Default)]
        public long id_negocio { get; set; }

        [Column("descricao")]
        [JsonProperty(PropertyName = "descricao", Required = Required.Always)]
        public string descricao { get; set; }

        public virtual Negocio negocio { get; set; }
                
        public virtual ICollection<Parte> partes { get; set; }
    }
}
