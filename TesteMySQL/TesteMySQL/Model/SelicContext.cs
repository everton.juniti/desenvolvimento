﻿using Microsoft.EntityFrameworkCore;

namespace TesteMySQL.Model
{
    public partial class SelicContext : DbContext
    {
        public SelicContext(DbContextOptions<SelicContext> options) : base(options)
        {

        }

        public DbSet<Negocio> Negocio { get; set; }
        public DbSet<Especificacao> Especificacao { get; set; }
        public DbSet<Parte> Parte { get; set; }
        public DbSet<Tipo> Tipo { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Negocio>(entity =>
            {
                entity.HasKey(negocio => negocio.id)
                    .HasName("PK_Negocio");

                entity.Property(negocio => negocio.id)
                    .HasColumnName("id");

                entity.Property(negocio => negocio.descricao)
                    .HasColumnName("descricao")
                    .HasMaxLength(50)
                    .IsRequired();

                entity.HasMany(negocio => negocio.especificacoes)
                    .WithOne(especificacao => especificacao.negocio)
                    .HasForeignKey(especificacao => especificacao.id_negocio)
                    .HasPrincipalKey(negocio => negocio.id);
            });

            modelBuilder.Entity<Especificacao>(entity =>
            {
                entity.HasKey(especificacao => especificacao.id)
                    .HasName("PK_Especificacao");

                entity.Property(especificacao => especificacao.id)
                    .HasColumnName("id");

                entity.Property(especificacao => especificacao.id_negocio)
                    .HasColumnName("id_negocio")
                    .IsRequired();

                entity.Property(especificacao => especificacao.descricao)
                    .HasColumnName("descricao")
                    .HasMaxLength(50)
                    .IsRequired();

                entity.HasMany(especificacao => especificacao.partes)
                    .WithOne(parte => parte.especificacao)
                    .HasForeignKey(parte => new { parte.id_negocio, parte.id_especificacao })
                    .HasPrincipalKey(especificacao => new { especificacao.id_negocio, especificacao.id });
            });

            modelBuilder.Entity<Parte>(entity =>
            {
                entity.HasKey(parte => new { parte.id_negocio, parte.id_especificacao, parte.id_tipo })
                    .HasName("PK_Parte");

                entity.Property(parte => parte.id_negocio)
                    .HasColumnName("id_negocio");

                entity.Property(parte => parte.id_especificacao)
                    .HasColumnName("id_especificacao");

                entity.Property(parte => parte.id_tipo)
                    .HasColumnName("id_tipo");

                entity.Property(parte => parte.descricao)
                    .HasColumnName("descricao")
                    .HasMaxLength(50)
                    .IsRequired();

                entity.HasOne(parte => parte.tipo)
                    .WithOne(tipo => tipo.parte)
                    .HasForeignKey<Tipo>(tipo => tipo.id)
                    .HasPrincipalKey<Parte>(parte => parte.id_tipo);
            });

            modelBuilder.Entity<Tipo>(entity =>
            {
                entity.HasKey(tipo => tipo.id)
                    .HasName("PK_Tipo");

                entity.Property(tipo => tipo.id)
                    .HasColumnName("id");

                entity.Property(tipo => tipo.descricao)
                    .HasColumnName("descricao")
                    .HasMaxLength(50)
                    .IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
