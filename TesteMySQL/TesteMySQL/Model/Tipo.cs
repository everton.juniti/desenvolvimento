﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteMySQL.Model
{
    [Table("tipo")]
    public class Tipo
    {
        [Key, Column("id")]
        public short id { get; set; }

        [Column("descricao")]
        public string descricao { get; set; }

        public virtual Parte parte { get; set; }
    }
}
