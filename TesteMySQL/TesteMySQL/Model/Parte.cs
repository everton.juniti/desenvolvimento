﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteMySQL.Model
{
    [Table("parte")]
    public class Parte
    {
        [Key, Column("id_negocio", Order = 0)]
        [JsonProperty(PropertyName = "id_negocio", Required = Required.Default)]
        public long id_negocio { get; set; }

        [Key, Column("id_especificacao", Order = 1)]
        [JsonProperty(PropertyName = "id_especificacao", Required = Required.Default)]
        public long id_especificacao { get; set; }

        [Key, Column("id_tipo", Order = 2)]
        [JsonProperty(PropertyName = "id_tipo", Required = Required.Always)]
        public short id_tipo { get; set; }

        [Column("descricao")]
        [JsonProperty(PropertyName = "descricao", Required = Required.Always)]
        public string descricao { get; set; }

        public virtual Especificacao especificacao { get; set; }

        public virtual Tipo tipo { get; set; }
    }
}
