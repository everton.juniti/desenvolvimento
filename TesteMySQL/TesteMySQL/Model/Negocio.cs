﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteMySQL.Model
{
    [Table("negocio")]
    public class Negocio
    {
        [Key, Column("id")]
        [JsonProperty(PropertyName = "id", Required = Required.Default)]
        public long id { get; set; }

        [Column("descricao")]
        [JsonProperty(PropertyName = "descricao", Required = Required.Always)]
        public string descricao { get; set; }

        public virtual ICollection<Especificacao> especificacoes { get; set; }
    }
}
