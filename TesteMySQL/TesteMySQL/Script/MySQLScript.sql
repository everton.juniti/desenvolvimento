drop table if exists dbpm400.parte;
drop table if exists dbpm400.tipo;
drop table if exists dbpm400.especificacao;
drop table if exists dbpm400.negocio;

create table dbpm400.negocio
(
	id bigint auto_increment, 
    descricao varchar(50),
    primary key PK_Negocio (id)
);

create table dbpm400.especificacao
(
	id bigint auto_increment,
    id_negocio bigint,
    descricao varchar(50),
    primary key PK_Especificacao (id),
    foreign key FK_Negocio (id_negocio) references dbpm400.negocio(id)
);

create table dbpm400.tipo
(
	id smallint auto_increment,
    descricao varchar(50),
    primary key PK_Tipo (id)
);

create table dbpm400.parte
(
	id_negocio bigint,
    id_especificacao bigint,
    id_tipo smallint,
    descricao varchar(50) not null,
    primary key PK_Parte (id_negocio, id_especificacao, id_tipo),
    foreign key FK_Especificacao (id_negocio, id_especificacao) references dbpm400.especificacao(id_negocio, id),
    foreign key FK_Tipo (id_tipo) references dbpm400.tipo(id)
);

insert into dbpm400.negocio(id, descricao) values (0, 'Teste 1');
insert into dbpm400.negocio(id, descricao) values (0, 'Teste 2');
select * from dbpm400.negocio;

insert into dbpm400.tipo(id, descricao) values (0, 'Cedente');
insert into dbpm400.tipo(id, descricao) values (0, 'Cessionário');
select * from dbpm400.tipo;

insert into dbpm400.especificacao(id, id_negocio, descricao) values (0, 1, 'Especificação 1');
insert into dbpm400.especificacao(id, id_negocio, descricao) values (0, 2, 'Especificação 2');
select * from dbpm400.especificacao;

insert into dbpm400.parte(id_negocio, id_especificacao, id_tipo, descricao) values (1, 1, 1, 'Parte 1');
insert into dbpm400.parte(id_negocio, id_especificacao, id_tipo, descricao) values (1, 1, 2, 'Parte 2');
insert into dbpm400.parte(id_negocio, id_especificacao, id_tipo, descricao) values (2, 2, 1, 'Parte 1');
insert into dbpm400.parte(id_negocio, id_especificacao, id_tipo, descricao) values (2, 2, 2, 'Parte 2');
select * from dbpm400.parte;

select * 
  from dbpm400.negocio as negocio 
 inner join dbpm400.especificacao as especificacao 
    on (negocio.id = especificacao.id_negocio)
 inner join dbpm400.parte as parte 
    on (especificacao.id_negocio = parte.id_negocio and especificacao.id = parte.id_especificacao)
 inner join dbpm400.tipo as tipo 
    on (parte.id_tipo = tipo.id) 
 where negocio.id = 1;