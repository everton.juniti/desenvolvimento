﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TesteMySQL.Model;

namespace TesteMySQL.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SelicController : ControllerBase
    {
        private readonly SelicContext _selicContext;

        public SelicController(SelicContext selicContext)
        {
            _selicContext = selicContext;
        }

        [HttpPost("negocios")]
        public async Task<ActionResult<Negocio>> PostNegocio([FromBody] Negocio negocio)
        {
            negocio.id = 0;
            _selicContext.Negocio.Add(negocio);
            await _selicContext.SaveChangesAsync();

            return CreatedAtAction("GetNegocio", new { id = negocio.id }, negocio);
        }

        [HttpGet("negocios")]
        public async Task<ActionResult<IEnumerable<Negocio>>> GetNegocios()
        {
            return await _selicContext.Negocio
                .Include(negocio => negocio.especificacoes)
                .ThenInclude(especificacao => especificacao.partes)
                .ThenInclude(parte => parte.tipo)
                .ToListAsync();
        }

        [HttpGet("negocios/{id}", Name = "GetNegocio")]
        public async Task<ActionResult<Negocio>> GetNegocio([FromRoute] long id)
        {
            var negocio = await _selicContext.Negocio.FindAsync(id);

            if (negocio == null)
            {
                return NotFound();
            }

            return negocio;
        }

        [HttpPost("negocios/{id}/especificacoes")]
        public async Task<ActionResult<Especificacao>> PostEspecificacao([FromRoute] long id, [FromBody] Especificacao especificacao)
        {
            var negocio = await _selicContext.Negocio.FindAsync(id);

            if (negocio == null)
            {
                return NotFound();
            }

            especificacao.id_negocio = negocio.id;
            especificacao.id = 0;
            _selicContext.Especificacao.Add(especificacao);
            await _selicContext.SaveChangesAsync();

            return CreatedAtAction("GetEspecificacao", new { id = especificacao.id_negocio, idEspecificacao = especificacao.id }, especificacao);
        }

        [HttpGet("negocios/{id}/especificacoes")]
        public async Task<ActionResult<IEnumerable<Especificacao>>> GetEspecificacoes([FromRoute] long id)
        {
            var negocio = await _selicContext.Negocio.FindAsync(id);

            if (negocio == null)
            {
                return NotFound();
            }

            return await _selicContext.Especificacao
                .Where(especificacao => especificacao.id_negocio == negocio.id)
                .Include(especificacao => especificacao.partes)
                .ThenInclude(parte => parte.tipo)
                .ToListAsync();
        }

        [HttpGet("negocios/{id}/especificacoes/{idEspecificacao}", Name = "GetEspecificacao")]
        public async Task<ActionResult<Especificacao>> GetEspecificacao([FromRoute] long id, [FromRoute] long idEspecificacao)
        {
            var negocio = await _selicContext.Negocio.FindAsync(id);

            if (negocio == null)
            {
                return NotFound();
            }

            var especificacao = await _selicContext.Especificacao.FindAsync(idEspecificacao);

            if (especificacao == null)
            {
                return NotFound();
            }

            return especificacao;
        }

        [HttpPost("negocios/{id}/especificacoes/{idEspecificacao}/partes")]
        public async Task<ActionResult<Parte>> PostParte([FromRoute] long id, [FromRoute] long idEspecificacao, 
            [FromBody] Parte parte)
        {
            var negocio = await _selicContext.Negocio.FindAsync(id);

            if (negocio == null)
            {
                return NotFound();
            }

            var especificacao = await _selicContext.Especificacao.FindAsync(idEspecificacao);

            if (especificacao == null)
            {
                return NotFound();
            }

            parte.id_negocio = negocio.id;
            parte.id_especificacao = especificacao.id;
            _selicContext.Parte.Add(parte);
            await _selicContext.SaveChangesAsync();

            return CreatedAtAction("GetParte", new { id = parte.id_negocio, idEspecificacao = parte.id_especificacao, idTipo = parte.id_tipo }, parte);
        }

        [HttpGet("negocios/{id}/especificacoes/{idEspecificacao}/partes")]
        public async Task<ActionResult<IEnumerable<Parte>>> GetPartes([FromRoute] long id, [FromRoute] long idEspecificacao)
        {
            var negocio = await _selicContext.Negocio.FindAsync(id);

            if (negocio == null)
            {
                return NotFound();
            }

            var especificacao = await _selicContext.Especificacao.FindAsync(idEspecificacao);

            if (especificacao == null)
            {
                return NotFound();
            }

            return await _selicContext.Parte
                .Where(parte => parte.id_negocio == negocio.id && parte.id_especificacao == especificacao.id)
                .Include(parte => parte.tipo)
                .ToListAsync();
        }

        [HttpGet("negocios/{id}/especificacoes/{idEspecificacao}/partes/{idTipo}", Name = "GetParte")]
        public async Task<ActionResult<Parte>> GetParte([FromRoute] long id, [FromRoute] long idEspecificacao, [FromRoute] short idTipo)
        {
            var negocio = await _selicContext.Negocio.FindAsync(id);

            if (negocio == null)
            {
                return NotFound();
            }

            var especificacao = await _selicContext.Especificacao.FindAsync(idEspecificacao);

            if (especificacao == null)
            {
                return NotFound();
            }

            var parte = await _selicContext.Parte.FindAsync(negocio.id, especificacao.id, idTipo);

            if (parte == null)
            {
                return NotFound();
            }

            return parte;
        }
    }
}
